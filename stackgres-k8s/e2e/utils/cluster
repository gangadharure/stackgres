#!/bin/sh

remove_cluster() {
  local RELEASE="$1"
  local NAMESPACE="$2"

  echo "Deleting release $RELEASE" 
  helm get manifest "$RELEASE" --namespace "$NAMESPACE" \
    | kubectl delete --namespace "$NAMESPACE" --ignore-not-found -f -
  helm get hooks "$RELEASE" --namespace "$NAMESPACE" \
    | kubectl delete --namespace "$NAMESPACE" --ignore-not-found -f -
  helm uninstall "$RELEASE" --namespace "$NAMESPACE"
  kubectl wait sts --namespace "$NAMESPACE" -l "cluster-name=$RELEASE" --all --for=delete --timeout="${E2E_TIMEOUT}s" 2>/dev/null || true
  kubectl wait pvc --namespace "$NAMESPACE" -l "cluster-name=$RELEASE" --all --for=delete --timeout="${E2E_TIMEOUT}s" 2>/dev/null || true
  kubectl wait pod --namespace "$NAMESPACE" -l "cluster-name=$RELEASE" --all --for=delete --timeout="${E2E_TIMEOUT}s" 2>/dev/null || true
  kubectl delete endpoints --namespace "$NAMESPACE" -l "cluster-name=$RELEASE" --all --ignore-not-found 2>/dev/null || true
}

remove_cluster_if_exists() {
  local RELEASE="$1"
  local NAMESPACE="$2"

  if helm get values "$RELEASE"  --namespace "$NAMESPACE" > /dev/null 2>&1
  then
    remove_cluster "$RELEASE" "$NAMESPACE"
  fi
}

create_or_replace_cluster() {
  local RELEASE="$1"
  local NAMESPACE="$2"
  local INSTANCES="$3"
  shift 3

  local OPERATOR_VERSION
  OPERATOR_VERSION="$(get_installed_operator_version)"
  if [ "$OPERATOR_VERSION" != "$STACKGRES_VERSION" ]
  then
    create_or_replace_cluster_for_version "$OPERATOR_VERSION" "$RELEASE" "$NAMESPACE" "$INSTANCES" "$@"
    return
  fi

  if helm get values "$RELEASE" --namespace "$NAMESPACE" > /dev/null 2>&1
  then
    helm upgrade "$RELEASE" --namespace "$NAMESPACE" "$CLUSTER_CHART_PATH" \
        --reuse-values \
        $E2E_CLUSTER_OPTS \
        --set cluster.instances="$INSTANCES" -f "$SPEC_VALUES_FILE" "$@"
  else
    create_namespace_if_not_exists "$NAMESPACE"
    helm install "$RELEASE" "$CLUSTER_CHART_PATH" \
      --namespace "$NAMESPACE" \
      $E2E_CLUSTER_OPTS \
      --set cluster.instances="$INSTANCES" -f "$SPEC_VALUES_FILE" "$@"
  fi
}

wait_cluster() {
  local RELEASE="$1"
  local NAMESPACE="$2"

  echo "Wait release $RELEASE" 
  wait_until eval '[ "$(kubectl get endpoints --namespace "$NAMESPACE" "$RELEASE" \
    --template="{{ range .subsets }}{{ range .addresses }}{{ printf \"%s\n\" .ip }}{{ end }}{{ end }}" 2>/dev/null \
    | wc -l)" -ge 1 ]'
}

has_cluster_generated_resources() {
  kubectl get sts -n "$CLUSTER_NAMESPACE" "$CLUSTER_NAME" > /dev/null 2>&1
}

create_or_replace_cluster_for_version() {
  local VERSION="$1"
  local RELEASE="$2"
  local NAMESPACE="$3"
  local INSTANCES="$4"
  shift 4

  local CLUSTER_HELM_URL
  CLUSTER_HELM_URL="$(get_cluster_helm_url "$VERSION")"
  local CLUSTER_HELM_TAR_PATH="$LOG_PATH/${CLUSTER_HELM_URL##*/}"
  if ! [ -f "$CLUSTER_HELM_TAR_PATH" ]
  then
    curl -f -s -L "$CLUSTER_HELM_URL" -o "$CLUSTER_HELM_TAR_PATH"
  fi
  local CLUSTER_HELM_PATH
  tar tzvf "$CLUSTER_HELM_TAR_PATH" > "$CLUSTER_HELM_TAR_PATH.log"
  CLUSTER_HELM_PATH="$(tr -s ' ' < "$CLUSTER_HELM_TAR_PATH.log" \
    | cut -d ' ' -f 6 | cut -d / -f 1 | head -n 1)"
  CLUSTER_HELM_PATH="$LOG_PATH/$CLUSTER_HELM_PATH"
  if ! [ -f "$CLUSTER_HELM_PATH/Chart.yaml" ]
  then
    rm -rf "$CLUSTER_HELM_PATH"
    tar xzf "$CLUSTER_HELM_TAR_PATH" -C "$LOG_PATH"
    if ! sed -i '/^kubeVersion:/d' "$CLUSTER_HELM_PATH/Chart.yaml"
    then
      rm -rf "$CLUSTER_HELM_PATH"
      return 1
    fi
  fi

  local TRANSFORMER="dont_transform"
  if [ "$(get_version_as_number "$VERSION")" -lt "$(get_version_as_number 1.0.0-alpha1)" ]
  then
    TRANSFORMER="transform_to_0_9"
  fi

  if helm get values "$RELEASE" --namespace "$NAMESPACE" > /dev/null 2>&1
  then
    eval "$(transform_params "$TRANSFORMER" helm upgrade "$RELEASE" --namespace "$NAMESPACE" "$CLUSTER_HELM_PATH" \
        --reuse-values \
        $E2E_CLUSTER_OPTS \
        --set cluster.instances="$INSTANCES" -f "$SPEC_VALUES_FILE" "$@")"
  else
    create_namespace_if_not_exists "$NAMESPACE"
    eval "$(transform_params "$TRANSFORMER" helm install "$RELEASE" "$CLUSTER_HELM_PATH" \
        --namespace "$NAMESPACE" \
        $E2E_CLUSTER_OPTS \
        --set cluster.instances="$INSTANCES" -f "$SPEC_VALUES_FILE" "$@")"
  fi
}

transform_to_0_9() {
  if printf '%s' "$1" | grep -q '^cluster\.metadata\.labels\.clusterPods'
  then
    printf '%s' "$1" | sed 's/^cluster\.metadata\.labels\.clusterPods\(.*\)/cluster.pods.metadata.labels\1/'
  elif printf '%s' "$1" | grep -q '^cluster\.metadata\.annotations\.clusterPods'
  then
    printf '%s' "$1" | sed 's/^cluster\.metadata\.annotations\.clusterPods\(.*\)/cluster.metadata.annotations.pods\1/'
  elif printf '%s' "$1" | grep -q '^cluster\.metadata\.annotations\.primaryService'
  then
    printf '%s' "$1" | sed 's/^cluster\.metadata\.annotations\.primaryService\(.*\)/cluster.postgresServices.primary.annotations\1/'
  elif printf '%s' "$1" | grep -q '^cluster\.metadata\.annotations\.replicasService'
  then
    printf '%s' "$1" | sed 's/^cluster\.metadata\.annotations\.replicasService\(.*\)/cluster.postgresServices.replicas.annotations\1/'
  elif [ "${1%=*}" = 'cluster.postgres.version' ]
  then
    printf '%s=%s' 'cluster.postgresVersion' "${1#*=}"
  elif printf '%s' '${1%=*}' | grep -q '^cluster.postgres.extensions'
  then
    printf "%s=%s" "$(printf '%s' "${1%=*}" \
      | sed 's/^cluster\.postgres\.extensions/cluster.postgresExtensions/')"
  elif printf '%s' '${1%=*}' | grep -q '^cluster.postgres.extensions'
  then
    printf "%s=%s" "$(printf '%s' "${1%=*}" \
      | sed 's/^cluster\.postgres\.extensions/cluster.postgresExtensions/')"
  elif [ "${1%=*}" = "cluster.initialData.restore.fromBackup.uid" ]
  then
    printf "%s=%s" "cluster.initialData.restore.fromBackup" "${1#*=}"
  else
    dont_transform "$1"
  fi
}

dont_transform() {
  printf "%s" "$1"
}