#!/bin/sh

prometheus_namespace() {
  echo "$CLUSTER_NAMESPACE-monitor"
}

prometheus_service() {
  echo "prometheus-kube-prometheus-prometheus"
}

install_prometheus_operator() {
  install_prometheus_operator_for_version 18.0.6 "$@"
}

install_prometheus_operator_for_version() {
  local KUBE_PROMETHEUS_STACK_VERSION="$1"
  shift

  if [ "$E2E_ENV" = "minishift" ]
  then
    eval "$(minishift oc-env)"
    oc adm policy add-scc-to-user anyuid -n "$(prometheus_namespace)" -z prometheus-prometheus-oper-admission
    oc adm policy add-scc-to-user anyuid -n "$(prometheus_namespace)" -z prometheus-grafana
    oc adm policy add-scc-to-user anyuid -n "$(prometheus_namespace)" -z prometheus-prometheus-oper-operator
    oc adm policy add-scc-to-user anyuid -n "$(prometheus_namespace)" -z prometheus-prometheus-oper-prometheus
  fi

  helm install prometheus "$E2E_PATH/helm/kube-prometheus-stack-$KUBE_PROMETHEUS_STACK_VERSION.tgz" \
    --create-namespace \
    --namespace "$(prometheus_namespace)" \
    --disable-openapi-validation \
    --set prometheusOperator.createCustomResource=false \
    --set alertmanager.enabled=false \
    --set kubeStateMetrics.enabled=false \
    --set nodeExporter.enabled=false \
    --set kubeDns.enabled=false \
    --set kubeProxy.enabled=false \
    --set kubeScheduler.enabled=false \
    --set coreDns.enabled=false \
    --set kubeControllerManager.enabled=false \
    --set kubeEtcd.enabled=false \
    "$@"

  wait_pods_running "$(prometheus_namespace)" 3
}

uninstall_prometheus_operator() {
  helm_cleanup_chart prometheus "$(prometheus_namespace)"
  k8s_async_cleanup_namespace "$(prometheus_namespace)"
}
